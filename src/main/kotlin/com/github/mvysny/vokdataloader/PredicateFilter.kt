package com.github.mvysny.vokdataloader

/**
 * A predicate filter which filters out all records not matching given [predicate].
 *
 * WARNING: only use this filter with in-memory data loaders such as [ListDataLoader].
 * Other [DataLoader] implementors will most probably not support this filter and
 * will throw an exception in [DataLoader.fetch] and [DataLoader.getCount].
 * @author Martin Vysny <mavi@vaadin.com>
 */
public data class PredicateFilter<T: Any>(val predicate: (T) -> Boolean) : Filter<T> {
    override fun test(t: T): Boolean = predicate(t)
}
